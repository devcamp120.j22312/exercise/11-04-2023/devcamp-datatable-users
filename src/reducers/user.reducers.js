import { FETCH_USERS_PENDING, FETCH_USERS_SUCCESS, SELECT_USER_HANDLER, USERS_PAGINATION_CHANGE } from "../constants/user.constant";

const initialState = {
    users: [],
    pending: false,
    error: null,
    userData: null,
    totalUser: 0,
    currentPage: 1
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_USERS_PENDING:
            state.pending = true;    
            break;
        case FETCH_USERS_SUCCESS:
            state.totalUser = action.totalUser;
            state.users = action.data;
            state.pending = false;
            break;
        case SELECT_USER_HANDLER:
            state.userData = action.data;
            break;
        case USERS_PAGINATION_CHANGE:
            state.currentPage = action.page;
            break;
    }
    return {...state}
}