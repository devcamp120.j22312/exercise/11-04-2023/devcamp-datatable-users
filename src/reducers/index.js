import { combineReducers } from "redux";
import { userReducer } from "./user.reducers";

const rootReducers = combineReducers ({
    userReducer
})

export default rootReducers