import { Button, TablePagination, Container, Grid, Paper, Table, TableCell, TableContainer, TableHead, TableRow, TableBody, Pagination, CircularProgress } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changePagination, fetchUser, selectUser } from "../actions/user.action";
import AlertDialog from "./modal";


const RegisterPage = () => {
    const dispatch = useDispatch();
    const { users, currentPage, totalUser, pending, userData } = useSelector((reduxData) => {
        return reduxData.userReducer;
    })

    const size = 2;
    const noPage = Math.ceil(totalUser / size);

    useEffect(() => {
        dispatch(fetchUser(size, currentPage))
    }, [currentPage])

    const onChangePagination = (event, value) => {
        dispatch(changePagination(value));
    }

    const onBtnDetailHandler = (value) => {
        console.log(value);
        dispatch(selectUser(value));
    }

    const [page, setPage] = useState(2);
    const [rowsPerPage, setRowsPerPage] = useState(10);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    }

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value));
        setPage(0);
    }

    return (
        <Container>
            <Grid container mt={5}>
                <TablePagination
                    component="div"
                    count={50}
                    page={page}
                    onPageChange={handleChangePage}
                    rowsPerPageOptions={[5, 10, 25, 50]}
                    rowsPerPage={rowsPerPage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
                {pending ?
                    <Grid item md={12} sm={12} lg={12} xs={12} textAlign="center">
                        <CircularProgress />
                    </Grid>
                    :
                    <>
                        <Grid item md={12} sm={12} lg={12} xs={12}>
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell>First Name</TableCell>
                                            <TableCell>Last Name</TableCell>
                                            <TableCell>Country</TableCell>
                                            <TableCell>Subject</TableCell>
                                            <TableCell>Customer Type</TableCell>
                                            <TableCell>Register Status</TableCell>
                                            <TableCell>Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {users.map((value, index) => {
                                            return <>
                                                <TableRow key={index}>
                                                    <TableCell>{value.id}</TableCell>
                                                    <TableCell>{value.firstname}</TableCell>
                                                    <TableCell>{value.lastname}</TableCell>
                                                    <TableCell>{value.country}</TableCell>
                                                    <TableCell>{value.subject}</TableCell>
                                                    <TableCell>{value.customerType}</TableCell>
                                                    <TableCell>{value.registerStatus}</TableCell>
                                                    <TableCell><Button variant="contained" onClick={() => onBtnDetailHandler(value)}>Details</Button></TableCell>

                                                </TableRow>
                                            </>
                                        })}
                                        {userData !== null ? <AlertDialog /> : null}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                            <Grid container md={12} sm={12} lg={12} xs={12} mt={5} justifyContent="center">
                                <Pagination count={noPage} defaultPage={currentPage} onChange={onChangePagination}
                                />
                            </Grid>
                        </Grid>
                    </>
                }
            </Grid>

        </Container>
    )
}
export default RegisterPage;
