import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { useState } from 'react';
import { useSelector, useDispatch } from "react-redux";
import { selectUser } from '../actions/user.action';

const AlertDialog =() =>{
    const dispatch = useDispatch();
    const {userData} = useSelector((reduxData) =>
    reduxData.userReducer
    );
    console.log(userData);
    const [open, setOpen] = useState(true);

    const handleClose = () => {
        setOpen(false);
        dispatch(selectUser(null))
    };
       return (
        <div>
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title">
            {`Information Of User`}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-description">
                       First Name: {userData.firstname}
                </DialogContentText>
                <DialogContentText id="alert-dialog-description">
                       Last Name: {userData.lastname}
                </DialogContentText>
                <DialogContentText id="alert-dialog-description">
                       Country: {userData.country}
                </DialogContentText>
                <DialogContentText id="alert-dialog-description">
                       Subject: {userData.subject}
                </DialogContentText>
                <DialogContentText id="alert-dialog-description">
                       Customer Type: {userData.customerType}
                </DialogContentText>
                <DialogContentText id="alert-dialog-description">
                       Register Status: {userData.registerStatus}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
            <Button onClick={handleClose}>Close</Button>
            </DialogActions>
        </Dialog>
        </div>
  );
}
export default AlertDialog;
